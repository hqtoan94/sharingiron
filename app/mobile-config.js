/**
 * Created by frt on 9/14/15.
 */
App.info({
    name: 'Restaurant',
    description: 'An Android app built with Meteor',
    version: '1.0.0'
});
App.accessRule('https://*.googleapis.com/*');
App.accessRule('https://*.google.com/*');
App.accessRule('https://*.gstatic.com/*');