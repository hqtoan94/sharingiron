
Router.configure({
	layoutTemplate: 'MasterLayout',
	loadingTemplate: 'Loading',
	notFoundTemplate: 'NotFound'
});

Router.route('/', {
  name: 'home',
  controller: 'HomeController',
  where: 'client'
});

Router.route('/menu', {
  name: 'menuPage',
  controller: 'MenuController',
  where: 'client'
});

Router.route('/location', {
  name: 'locationPage',
  controller: 'LocationController',
  where: 'client'
});

Router.route('/contact', {
  name: 'contactPage',
  controller: 'ContactController',
  where: 'client'
});

Router.route('/about', {
  name: 'aboutPage',
  controller: 'AboutController',
  where: 'client'
});

Router.route('/feedback', {
  name: 'feedbackPage',
  controller: 'FeedbackController',
  where: 'client'
});

Router.route('CategoryItem', {
  path: '/menu/:_id'
});

Router.route('MapLocation', {
  path: '/location/:_id',
  onBeforeAction: function() {
    GoogleMaps.load({
      key: "AIzaSyD_FZUKDqeulHAkKP83YG28NewWWRvZCFg"
    });
    return this.next();
  }
});