Categories = new SQL.Collection('Categories');
Category = new SQL.Collection('Category');

Locations = new SQL.Collection('Locations');
TimeOpen = new SQL.Collection('TimeOpen');

Orders = new SQL.Collection('Orders');

Categories.createTable({
	id: ['$string', '$primary'],
	name: ['$string'],
	title: ['$string'],
	image_link: ['$string']
});

Category.createTable({
	id: ['$string', '$primary'],
	name: ['$string'],
	title: ['$string'],
	image_link: ['$string'],
	price: ['$float'],
	ofCategories: ['$string']
});

Locations.createTable({
	id: ['$string', '$primary'],
	address: ['$string'],
	link: ['$string'],
	phone: ['$string'],
	lat: ['$string'],
	lng: ['$string']
});

TimeOpen.createTable({
	id: ['$number', '$primary'],
	time_open: ['$string'],
	ofLocation: ['$string']
});

Orders.createTable({
	id: ['$string'],
	counter: ['$string']
});


