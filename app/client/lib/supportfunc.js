/**
 * Created by frt on 9/24/15.
 */
setPageHeight = function(page){
    var wHeight = $(window).height();
    var h = $(page).height();
    if(h < wHeight)
    {
        $(page).height(wHeight);
    }
}

scrollTop = function(){
    $('html,body').scrollTop(0);
}

backToWithoutID = function(template){
    Router.go(template);
}