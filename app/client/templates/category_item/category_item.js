/*****************************************************************************/
/* CategoryItem: Event Handlers */
/*****************************************************************************/
Template.CategoryItem.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('menuPage');
    },
    'tap .add-btn': function(){
        $('.total-order').html('$' + updateTotalOrder());
    },
    'tap .total-order': function(){
        Session.set('navigating', 'next');

        $.each($('.item'), function(){
            //Orders.insert({id: this.id, counter: $(this).find('.quality').val()});
            alasql("INSERT INTO Orders (id, counter) VALUES ('" + this.id + "', '" + $(this).find('.quality').val() + "')");
        });

        console.log(Orders.select().fetch());

        var id = Router.current().params._id;
        //Router.go('checkoutYourOrder', {_id: id});
    }
});

var item_id = "";
function updateTotalOrder(){
    var total = 0;
    $.each($('.item'), function(index){
        var price = +$(this).find('.cat-price').html().replace('$', '');
        var quality = +$(this).find('.quality').val();

        total += price*quality;
    });
    return total;
}

/*****************************************************************************/
/* CategoryItem: Helpers */
/*****************************************************************************/
Template.CategoryItem.helpers({
    selected: function(){
        item_id = Router.current().params._id;
        return Category.select().where("ofCategories = ?", item_id).fetch();
    },
    category_title: function(){
        item_id = Router.current().params._id;
        return Categories.select().where("id = ?", item_id).fetch()[0];
    }
});

/*****************************************************************************/
/* CategoryItem: Lifecycle Hooks */
/*****************************************************************************/
Template.CategoryItem.onCreated(function () {
});

Template.CategoryItem.onRendered(function () {

});

Template.CategoryItem.onDestroyed(function () {
});
