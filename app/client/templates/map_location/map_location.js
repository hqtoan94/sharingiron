/*****************************************************************************/
/* MapLocation: Event Handlers */
/*****************************************************************************/
Template.MapLocation.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('locationPage');
    },
});

/*****************************************************************************/
/* MapLocation: Helpers */
/*****************************************************************************/
Template.MapLocation.helpers({
    loc: function(){
        var id = Router.current().params._id;
        return Locations.select().where("id = ?", id).fetch()[0];
    },
    exampleMapOptions: function(lat, lng) {
        // Make sure the maps API has loaded
        if (GoogleMaps.loaded()) {
            // Map initialization options
            var id = Router.current().params._id;
            var item = Locations.select().where("id = ?", id).fetch()[0];
            return {
                center: new google.maps.LatLng(item.lat, item.lng),
                zoom: 16
            };
        }
        else
        {
            GoogleMaps.load();
        }
    }
});

/*****************************************************************************/
/* MapLocation: Lifecycle Hooks */
/*****************************************************************************/
Template.MapLocation.onCreated(function () {
    GoogleMaps.ready('exampleMap', function(map) {
        // Add a marker to the map once it's ready
        var marker = new google.maps.Marker({
            position: map.options.center,
            map: map.instance
        });
    });
});

Template.MapLocation.onRendered(function () {
    setPageHeight('.page');
    scrollTop();
});

Template.MapLocation.onDestroyed(function () {
});
