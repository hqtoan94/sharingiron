/*****************************************************************************/
/* CheckoutYourLocation: Event Handlers */
/*****************************************************************************/
Template.CheckoutYourLocation.events({
});

/*****************************************************************************/
/* CheckoutYourLocation: Helpers */
/*****************************************************************************/
Template.CheckoutYourLocation.helpers({
});

/*****************************************************************************/
/* CheckoutYourLocation: Lifecycle Hooks */
/*****************************************************************************/
Template.CheckoutYourLocation.onCreated(function () {
});

Template.CheckoutYourLocation.onRendered(function () {
});

Template.CheckoutYourLocation.onDestroyed(function () {
});
