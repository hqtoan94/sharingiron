/*****************************************************************************/
/* CheckoutYourOrder: Event Handlers */
/*****************************************************************************/
Template.CheckoutYourOrder.events({
    'tap .back-btn': function(){
        Session.set('navigating', 'back');
        var id = Router.current().params._id;
        Router.go('categoryItem', {_id: id});
    }
});

updateTotalOrder = function(){
    var total = 0;
    $.each($('.checkout-item'), function(index){
        var price = +$(this).find('.check-out-item-single-price').html().replace('$', '');
        var quality = +$(this).find('.numeric').val();
        var it = price*quality;

        var subTotal = $($('.checkout-item-subtotal').get(index));
        subTotal.html('$' + it);

        total += it;
    });
    // up date total

    // check fee
    var deliveryFee = 0;
    if ($.isNumeric(parseFloat($('#deliveryPrice').text()))){
        deliveryFee = parseFloat($('#deliveryPrice').text().toString().slice(1));
    }
    total += deliveryFee;
    // update total final
    $('#totalPrice').html('$'+ total);
    $('.total-order').html('$'+ total);
    $('#subTotalPrice').html('$' + total);
};
/*****************************************************************************/
/* CheckoutYourOrder: Helpers */
/*****************************************************************************/
Template.CheckoutYourOrder.helpers({
    selected: function(){

    },
    total_order: function(){

    }
});

/*****************************************************************************/
/* CheckoutYourOrder: Lifecycle Hooks */
/*****************************************************************************/
Template.CheckoutYourOrder.onCreated(function () {
});

Template.CheckoutYourOrder.onRendered(function () {
});

Template.CheckoutYourOrder.onDestroyed(function () {
});
