/*****************************************************************************/
/* CheckoutProvideYourDetails: Event Handlers */
/*****************************************************************************/
Template.CheckoutProvideYourDetails.events({
});

/*****************************************************************************/
/* CheckoutProvideYourDetails: Helpers */
/*****************************************************************************/
Template.CheckoutProvideYourDetails.helpers({
});

/*****************************************************************************/
/* CheckoutProvideYourDetails: Lifecycle Hooks */
/*****************************************************************************/
Template.CheckoutProvideYourDetails.onCreated(function () {
});

Template.CheckoutProvideYourDetails.onRendered(function () {
});

Template.CheckoutProvideYourDetails.onDestroyed(function () {
});
