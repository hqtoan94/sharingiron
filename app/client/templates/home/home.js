/*****************************************************************************/
/* Home: Event Handlers */
/*****************************************************************************/

Template.Home.events({
	'tap .home-logo': function(){

	},
	'tap .menu': function() {
		Session.set("navigating", "next");
		Router.go('menuPage');
	},
	'tap .location': function() {
		Session.set("navigating", "next");
		Router.go('locationPage');
	},
	'tap .contact': function() {
		Session.set("navigating", "next");
		Router.go('contactPage');
	},
	'tap .about': function() {
		Session.set("navigating", "next");
		Router.go('aboutPage');
	},
	'tap .feedback': function() {
		Session.set("navigating", "next");
		Router.go('feedbackPage');
	}
});
Categories.insert({id: "quesadillas", name: 'Quesadillas', title: 'Quesadillas', image_link: 'quesadillas.png'}).save();
Categories.insert({id: "burrito", name: "Burritos", title: "Burritos - wheat tortillas available", image_link: "burrito.png"}).save();
Categories.insert({id: "enchiladas", name: "Enchiladas", title: "Enchiladas", image_link: "enchiladas.png"}).save();
Categories.insert({id: "cheese_crisp", name: "Cheese Crisp", title: "Cheese Crisp", image_link: "cheese-crisp.png"}).save();
Categories.insert({id: "tacos", name: "Tacos", title: "Tacos", image_link: "tacos.png"}).save();
Categories.insert({id: "tostadas", name: "Tostadas", title: "Tostadas", image_link: "tostadas.png"}).save();

Category.insert({"id": "chimichanga", "name": "Chimichanga", "title": "Deep fried burro with sour cream and guacamole", "image_link": "burrito/burritos-chimichanga.png", "price": 3.9, "ofCategories": "burrito"}).save();
Category.insert({"id": "carnitas", "name": "Carnitas", "title": "Marinated pork griled with onions, peepers, tomatoes and guacamole", "image_link": "burrito/burritos-carnitas.png", "price": 3.9, "ofCategories": "burrito"}).save();
Category.insert({"id": "cerne_asada", "name": "Cerne Asada", "title": "Lean marinated and char broiled beef with fresh guacamole and salsa fresca", "image_link": "burrito/burritos-de-carne-asada.png", "price": 3.9, "ofCategories": "burrito"}).save();

Locations.insert({id: "location01", address: "5421 West Glendale Avenue Glendale, AZ 85301", link: "location1.png", phone: "623-934-8888", lat: "33.5382958", lng: "-112.1759478"}).save();
Locations.insert({id: "location02", address: "6661 West Bell Road Glendale, AZ 85308", link: "location2.png", phone: "623-487-1111", lat: "33.6370916", lng: "-112.201447"}).save();
Locations.insert({id: "location03", address: "2340 West Bell Road Phoenix, AZ 85023", link: "location3.png", phone: "602-863-7777", lat: "33.6403418", lng: "-112.1106724"}).save();
Locations.insert({id: "location04", address: "17239 North Litchfield Road Surprise, AZ 85374", link: "location4.png", phone: "623-546-5555", lat: "33.6410254", lng: "-112.3583862"}).save();
Locations.insert({id: "location05", address: "8285 W. Union Hills Dr Glendale, Az 85308", link: "location5.png", phone: "623-878-4444", lat: "33.652379", lng: "-112.2358852"}).save();

TimeOpen.insert({id: 1, time_open: "Monday - Thursday: 10am - 9pm", ofLocation: "location01"}).save();
TimeOpen.insert({id: 2, time_open: "Friday: 10am - 10pm", ofLocation: "location01"}).save();
TimeOpen.insert({id: 3, time_open: "Saturday: 8am - 10pm", ofLocation: "location01"}).save();
TimeOpen.insert({id: 4, time_open: "Sunday: 8am - 7pm", ofLocation: "location01"}).save();

TimeOpen.insert({id: 5, time_open: "Monday - Wednesday: 10am - 9pm", ofLocation: "location02"}).save();
TimeOpen.insert({id: 6, time_open: "Thursday: 10am - 10pm", ofLocation: "location02"}).save();
TimeOpen.insert({id: 7, time_open: "Friday: 10am - 10pm", ofLocation: "location02"}).save();
TimeOpen.insert({id: 8, time_open: "Saturday: 8am - 10pm", ofLocation: "location02"}).save();
TimeOpen.insert({id: 9, time_open: "Sunday: 8am - 9pm", ofLocation: "location02"}).save();

TimeOpen.insert({id: 10, time_open: "Monday - Wednesday: 10am - 9pm", ofLocation: "location03"}).save();
TimeOpen.insert({id: 11, time_open: "Thursday: 10am - 10pm", ofLocation: "location03"}).save();
TimeOpen.insert({id: 12, time_open: "Friday: 10am - 10pm", ofLocation: "location03"}).save();
TimeOpen.insert({id: 13, time_open: "Saturday: 8am - 10pm", ofLocation: "location03"}).save();
TimeOpen.insert({id: 14, time_open: "Sunday: 8am - 7pm", ofLocation: "location03"});

TimeOpen.insert({id: 15, time_open: "Monday - Thursday: 10am - 9pm", ofLocation: "location04"}).save();
TimeOpen.insert({id: 16, time_open: "Friday - Saturday: 10am - 10pm", ofLocation: "location04"}).save();
TimeOpen.insert({id: 17, time_open: "Sunday: 10am - 9pm", ofLocation: "location04"}).save();

TimeOpen.insert({id: 18, time_open: "Monday - Wednesday: 10am - 9pm", ofLocation: "location05"}).save();
TimeOpen.insert({id: 19, time_open: "Thursday: 10am - 10pm", ofLocation: "location05"}).save();
TimeOpen.insert({id: 20, time_open: "Friday: 10am - 10pm", ofLocation: "location05"}).save();
TimeOpen.insert({id: 21, time_open: "Saturday: 8am - 10pm", ofLocation: "location05"}).save();
TimeOpen.insert({id: 22, time_open: "Sunday: 8am - 9pm", ofLocation: "location05"}).save();

/*****************************************************************************/
/* Home: Helpers */
/*****************************************************************************/
Template.Home.helpers({
});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.onCreated(function () {
});

Template.Home.onRendered(function () {
  setPageHeight('.page');
  scrollTop();
});

Template.Home.onDestroyed(function () {
});