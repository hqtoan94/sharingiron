/*****************************************************************************/
/* LocationPage: Event Handlers */
/*****************************************************************************/
Template.LocationPage.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('home');
    },
    'tap .location-icon': function(){
        Session.set("navigating", "next");
        var id = this.id;
        Router.go('MapLocation', {_id: id});
    }
});

/*****************************************************************************/
/* LocationPage: Helpers */
/*****************************************************************************/
Template.LocationPage.helpers({
    location: function(){
        return Locations.select().fetch();
    },
    opentime: function(locat){
        return TimeOpen.select().where("ofLocation = ?", locat).fetch();
    }
});

/*****************************************************************************/
/* LocationPage: Lifecycle Hooks */
/*****************************************************************************/
Template.LocationPage.onCreated(function () {
});

Template.LocationPage.onRendered(function () {
});

Template.LocationPage.onDestroyed(function () {
});
