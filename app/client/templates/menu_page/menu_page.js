/*****************************************************************************/
/* MenuPage: Event Handlers */
/*****************************************************************************/
Template.MenuPage.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('home');
    },
    'tap .item': function(e){
        var id = this.id;
        Session.set("navigating", "next");
        Router.go('CategoryItem', {_id: id});
    }
});

/*****************************************************************************/
/* MenuPage: Helpers */
/*****************************************************************************/
Meteor.subscribe("loadCategories");
Template.MenuPage.helpers({
	'categories': function(){
        var temp = Categories.select().fetch();
        return temp;
   }
});

/*****************************************************************************/
/* MenuPage: Lifecycle Hooks */
/*****************************************************************************/
Template.MenuPage.onCreated(function () {
});

Template.MenuPage.onRendered(function () {
});

Template.MenuPage.onDestroyed(function () {
});
