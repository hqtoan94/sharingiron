/*****************************************************************************/
/* AboutPage: Event Handlers */
/*****************************************************************************/
Template.AboutPage.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('home');
    }
});

/*****************************************************************************/
/* AboutPage: Helpers */
/*****************************************************************************/
Template.AboutPage.helpers({
});

/*****************************************************************************/
/* AboutPage: Lifecycle Hooks */
/*****************************************************************************/
Template.AboutPage.onCreated(function () {
});

Template.AboutPage.onRendered(function () {
    setPageHeight('.page');
    scrollTop();
});

Template.AboutPage.onDestroyed(function () {
});