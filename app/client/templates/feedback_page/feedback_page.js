/*****************************************************************************/
/* FeedbackPage: Event Handlers */
/*****************************************************************************/
Template.FeedbackPage.events({
    'tap .back-btn': e => {
        Session.set('navigating', 'back');
        backToWithoutID('home');
    },
    'tap .rate-star': e => {
    	let index = $(e.currentTarget).index();
        $('.rate-star').removeClass('active fa-star').addClass('fa-star-o').each(function (i) {
            if(index > i)
                $(this).addClass('fa-star active').removeClass('fa-star-o');
        });
    }
});

/*****************************************************************************/
/* FeedbackPage: Helpers */
/*****************************************************************************/
Template.FeedbackPage.helpers({
});

/*****************************************************************************/
/* FeedbackPage: Lifecycle Hooks */
/*****************************************************************************/
Template.FeedbackPage.onCreated(function () {
});

Template.FeedbackPage.onRendered(function () {
    setPageHeight('.page');
    scrollTop();
});

Template.FeedbackPage.onDestroyed(function () {
});
