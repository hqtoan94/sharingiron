/*****************************************************************************/
/* ContactPage: Event Handlers */
/*****************************************************************************/
Template.ContactPage.events({
    'tap .back-btn': function(e){
        Session.set("navigating", "back");
        backToWithoutID('home');
    },
    'tap .catering-intro-contactUs': function(){
        Session.set("navigating", "next");
        Router.go('locationPage');
    }
});

/*****************************************************************************/
/* ContactPage: Helpers */
/*****************************************************************************/
Template.ContactPage.helpers({
});

/*****************************************************************************/
/* ContactPage: Lifecycle Hooks */
/*****************************************************************************/
Template.ContactPage.onCreated(function () {
});

Template.ContactPage.onRendered(function () {
});

Template.ContactPage.onDestroyed(function () {
});
