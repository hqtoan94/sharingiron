Template.MasterLayout.helpers({
});

Template.MasterLayout.events({

});

var nextInitiator = null, initiator = null;
Deps.autorun(function() {
  // add a dep
  Router.current();
  
  initiator = nextInitiator;
  nextInitiator = null;
});

var insert = function(trans, node, next){
	$.Velocity.hook(node, "translateX", trans);
	node.insertBefore(next).velocity({
    translateX: '0%'
  }, {
    duration: 500,
    easing: 'ease-in-out',
    queue: false
  });
};

var remove = function(trans, node){
	node.velocity({
    translateX: trans
  }, {
    duration: 500,
    easing: 'ease-in-out',
    queue: false,
    complete: function() {
      return node.remove();
    }
  });
}

Template.MasterLayout.rendered = function(){
	this.find(".main-container")._uihooks = {
        insertElement: function(node, next) {
            var nav = Session.get("navigating");

            var $node = $(node);

            if (nav === "back") {
              insert("-100%", $node, next);
            } else if (nav === "next"){
                insert("100%", $node, next);
            } else {
              return $node.insertBefore(next);
            }
        },
        removeElement: function(node) {
            var nav = Session.get("navigating");

            var $node = $(node);
            if (nav === "next") {
              remove("-100%", $node);
            } else if (nav === "back") {
              remove("100%", $node);
            } else {
              return $node.remove();
            }
        }
  };
};